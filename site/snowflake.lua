nixpkgs "import ./nix/nixpkgs.nix {}"

nixrule {
    name = "sassc",
    nixexpr = "nixpkgs.sassc",
}

genrule {
    name = "stylesheet",
    inputs = {
        ":sassc",
        "+src/stylesheet.scss",
        "+vendor/reset.scss",
    },
    outputs = { "@stylesheet.css" },
    command = [[
        export PATH="$PATH:$(loc :sassc)/bin"
        sassc -p 10 "$(loc +src/stylesheet.scss)" "$(loc @stylesheet.css)"
    ]],
}

local pages = {
    "genrule",
    "index",
    "install",
    "intro",
    "labels",
    "nixpkgs",
    "nixrule",
}

for _, page in ipairs(pages) do
    local command = [[
        cat "$(loc +src/header.html)"                                       \
            "$(loc +src/]] .. page .. [[.html)"                             \
            "$(loc +src/footer.html)"                                       \
            > "$(loc @]] .. page .. [[.html)"
    ]]
    genrule {
        name = page,
        inputs = {
            "+src/header.html",
            "+src/" .. page .. ".html",
            "+src/footer.html",
        },
        outputs = { "@" .. page .. ".html" },
        command = command,
    }
end

do
    local inputs = { }
    local outputs = { }
    local command = ""

    table.insert(inputs, ":stylesheet")
    table.insert(outputs, "@stylesheet.css")
    command = command .. [[cp "$(loc :stylesheet)/stylesheet.css" "$(loc @stylesheet.css)"]] .. "\n"

    for _, page in ipairs(pages) do
        table.insert(inputs, ":" .. page)
        table.insert(outputs, "@" .. page .. ".html")
        command = command .. [[cp "$(loc :]] .. page .. [[)/]] .. page .. [[.html" "$(loc @]] .. page .. [[.html)"]] .. "\n"
    end

    genrule {
        name = "site",
        inputs = inputs,
        outputs = outputs,
        command = command,
    }
end
