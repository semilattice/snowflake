__snowflake.nixpkgs = "import <nixpkgs> {}"
__snowflake.derivations = { }

local function require_table(caller, name, value)
    if type(value) ~= "table" then
        error(caller .. ": " .. name .. " must be a table")
    end
end

local function require_string(caller, name, value)
    if type(value) ~= "string" then
        error(caller .. ": " .. name .. " must be a string")
    end
end

local function require_string_table(caller, name, value)
    require_table(caller, name, value)
    for i, element in ipairs(value) do
        require_string(caller, name .. " element #" .. i, element)
    end
end

function quote_nix(str)
    require_string("quote_nix", "argument", str)
    str = string.gsub(str, "\\", "\\\\")
    str = string.gsub(str, "\"", "\\\"")
    str = string.gsub(str, "\n", "\\n")
    return "\"" .. str .. "\""
end

function quote_shell(str)
    require_string("quote_shell", "argument", str)
    return "${nixpkgs.lib.strings.escapeShellArg " .. quote_nix(str) .. "}"
end

function parse_input_label(label, sources, dependencies)
    if string.sub(label, 1, 1) == "+" then
        table.insert(sources, string.sub(label, 2))
        return
    end

    if string.sub(label, 1, 1) == ":" then
        -- If in addition to a derivation name the label specifies a file name,
        -- ignore the file name. The dependency is only the derivation.
        local derivation = label:match("%S+"):sub(2)
        table.insert(dependencies, derivation)
        return
    end

    error("parse_input_label: invalid syntax")
end

function parse_output_label(label, outputs)
    if string.sub(label, 1, 1) == "@" then
        table.insert(outputs, string.sub(label, 2))
        return
    end

    error("parse_output_label: invalid syntax")
end

function nixpkgs(nixexpr)
    require_string("nixpkgs", "argument", nixexpr)
    __snowflake.nixpkgs = nixexpr
end

function nixrule(rule)
    require_table("nixrule", "argument", rule)
    require_string("nixrule", "`name'", rule.name)
    require_string("nixrule", "`nixexpr'", rule.nixexpr)

    if string.match(rule.name, "^[A-Za-z0-9_-]+$") == nil then
        error("nixrule: invalid rule name")
    end

    if __snowflake.derivations[rule.name] ~= nil then
        error("nixrule: derivation `" .. rule.name .. "' was already defined")
    end

    __snowflake.derivations[rule.name] = rule.nixexpr
end

function genrule(rule)
    require_table("genrule", "argument", rule)
    require_string("genrule", "`name'", rule.name)
    require_string_table("genrule", "`inputs'", rule.inputs)
    require_string_table("genrule", "`outputs'", rule.outputs)
    require_string("genrule", "`command'", rule.command)

    local sources = { }
    local dependencies = { }
    local outputs = { }

    for _, label in ipairs(rule.inputs) do
        parse_input_label(label, sources, dependencies)
    end

    for _, label in ipairs(rule.outputs) do
        parse_output_label(label, outputs)
    end

    local nixexpr = "\n"
    local function line(s)
        nixexpr = nixexpr .. s .. "\n"
    end

    -- Not using stdenv, because it adds a lot of overhead that we do not need
    -- for granular builds.
    line([[builtins.derivation {]])

    line([[    name = ]] .. quote_nix(rule.name) .. [[;]])

    -- TODO: Should we always use the current system?
    line([[    system = builtins.currentSystem;]])

    -- Generate a builder for the rule.
    line([[    builder = nixpkgs.writeTextFile {]])
    line([[        name = "builder.sh";]])
    line([[        executable = true;]])
    line([[        text = ""]])

    -- sh is always available, so use it.
    line([[            + "#!/bin/sh\n"]])
    line([[            + "set -o errexit nounset pipefail\n"]])

    -- Only put coreutils in path, everything else can be put in path by the
    -- command itself, or resolved explicitly using loc.
    line([[            + "export PATH=${nixpkgs.lib.strings.escapeShellArg nixpkgs.coreutils}/bin\n"]])

    -- The loc function takes a label and returns the path of the label. Only
    -- labels that are specified as inputs and outputs can be passed; anything
    -- else will cause the build to fail.
    line([[            + "loc() {\n"]])
    line([[            + "    case \"$1\" in\n"]])
    for _, source in ipairs(sources) do
        line([[            + "    ]] .. quote_shell("+" .. source) .. [[)\n"]])
        line([[            + "        echo \"$src\"/]] .. quote_shell(source) .. [[\n"]])
        line([[            + "        ;;\n"]])
    end
    for _, dependency in ipairs(dependencies) do
        line([[            + "    ]] .. quote_shell(":" .. dependency) .. [[)\n"]])
        line([[            + "        if [ -z \"$2\" ]; then\n"]])
        line([[            + "            echo '${snowflake-]] .. dependency .. [[}'\n"]])
        line([[            + "        else\n"]])
        line([[            + "            echo '${snowflake-]] .. dependency .. [[}'/\"$2\"\n"]])
        line([[            + "        fi\n"]])
        line([[            + "        ;;\n"]])
    end
    for _, output in ipairs(outputs) do
        line([[            + "    ]] .. quote_shell("@" .. output) .. [[)\n"]])
        line([[            + "        echo \"$out\"/]] .. quote_shell(output) .. [[\n"]])
        line([[            + "        ;;\n"]])
    end
    line([[            + "    *)\n"]])
    line([[            + "        >&2 echo \"Unknown label \\`$1'\"\n"]])
    line([[            + "        ;;\n"]])
    line([[            + "    esac\n"]])
    line([[            + "}\n"]])

    -- Create the output directories and write the command.
    line([[            + "mkdir \"$out\"\n"]])
    for _, output in ipairs(outputs) do
        line([[            + "mkdir -p \"$out/$(dirname ]] .. quote_shell(output) .. [[)\"\n"]])
    end
    line([[            + ]] .. quote_nix(rule.command) .. [[;]])
    -- TODO: Verify that all outputs are created.

    line([[    };]])

    -- Add the src attribute so that the sources will be available, and filter
    -- the sources appropriately.
    line([[    src =]])
    line([[        let]])
    line([[            sources = []])
    for _, source in ipairs(sources) do
        -- TODO: source should be escaped.
        line([[                (toString (./. + ]] .. quote_nix("/" .. source) .. [[))]])
    end
    line([[            ];]])
    line([[            filter = p: t:]])
    line([[                t == "directory" ||]])
    line([[                builtins.any (s: s == p) sources;]])
    line([[        in]])
    line([[            builtins.filterSource filter ./.;]])

    -- Add the dependencies as referenced derivations.
    line([[    buildInputs = []])
    line([[        nixpkgs.coreutils]])
    for _, dependency in ipairs(dependencies) do
        line([[        snowflake-]] .. dependency)
    end
    line([[    ];]])

    line([[}]])

    nixrule {
        name = rule.name,
        nixexpr = nixexpr,
    }
end
