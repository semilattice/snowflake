nixrule {
    name = "gnu-cobol",
    nixexpr = "nixpkgs.gnu-cobol",
}

genrule {
    name = "test-case",
    inputs = { "+hello-world.cob", ":gnu-cobol" },
    outputs = { "@hello-world" },
    command = [[
        export PATH="$PATH:$(loc :gnu-cobol)/bin"
        cobc -x -o "$(loc @hello-world)" "$(loc +hello-world.cob)"
    ]],
}
