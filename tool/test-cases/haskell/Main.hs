module Main
    ( main
    ) where

import Greet (greet)

import qualified Data.Text as Text
import qualified Data.Text.IO as Text.IO

main :: IO ()
main = Text.IO.putStrLn . greet . Text.pack $ "world"
