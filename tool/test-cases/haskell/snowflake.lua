nixrule {
    name = "ghc",
    nixexpr = [[
        nixpkgs.haskell.packages.ghc843.ghcWithPackages
            (p: [])
    ]]
}

local function hs_module(rule)
    local name = rule.name
    local inputs = { ":ghc", "+" .. name .. ".hs" }
    local imports = ""
    for _, import in ipairs(rule.imports) do
        table.insert(inputs, ":" .. import)
        imports = imports .. [[ -i"$(loc :]] .. import .. [[)"]]
    end
    genrule {
        name = name,
        inputs = inputs,
        outputs = { "@" .. name .. ".o", "@" .. name .. ".hi" },
        command = [[
            export PATH="$PATH:$(loc :ghc)/bin"
            ghc                                                             \
                -c                                                          \
                -odir "$(dirname "$(loc @]] .. name .. [[.o)")"             \
                -hidir "$(dirname "$(loc @]] .. name .. [[.hi)")"           \
                ]] .. imports .. [[                                         \
                "$(loc +]] .. name .. [[.hs)"
        ]],
    }
end

hs_module { name = "Greet", imports = { "Shout" } }
hs_module { name = "Main",  imports = { "Greet" } }
hs_module { name = "Shout", imports = { } }

genrule {
    name = "test-case",
    inputs = { ":ghc", ":Greet", ":Main", ":Shout" },
    outputs = { "@program" },
    command = [[
        export PATH="$PATH:$(loc :ghc)/bin"
        ghc                                                                 \
            -o "$(loc @program)"                                            \
            -package text                                                   \
            "$(loc :Greet)/Greet.o"                                         \
            "$(loc :Main)/Main.o"                                           \
            "$(loc :Shout)/Shout.o"                                         \
    ]],
}
