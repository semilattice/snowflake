module Shout
    ( shout
    ) where

import Data.Text (Text)

import qualified Data.Text as Text

shout :: Text -> Text
shout = Text.toUpper
