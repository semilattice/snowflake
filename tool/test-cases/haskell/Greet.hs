{-# LANGUAGE OverloadedStrings #-}

module Greet
    ( greet
    ) where

import Shout (shout)

import Data.Semigroup ((<>))
import Data.Text (Text)

greet :: Text -> Text
greet e = shout $ "Hello, " <> e <> "!"
