let
    nixpkgs = import ./nix/nixpkgs.nix {};
in
    nixpkgs.stdenv.mkDerivation {
        name = "snowflake";
        src = ./.;
        buildInputs = [
            nixpkgs.lua5_3
            nixpkgs.xxd
        ];
        configurePhase = ''
            true
        '';
        buildPhase = ''
            mkdir build

            mkdir gen
            cat src/initialize.lua | xxd -i > gen/initialize.lua.inc
            cat src/finalize.lua   | xxd -i > gen/finalize.lua.inc

            gcc                                                             \
                -std=c11 -Wall -Wextra -Wpedantic -Werror                   \
                -o build/snowflake                                          \
                src/snowflake.c                                             \
                -llua
        '';
        installPhase = ''
            mkdir $out
            install build/snowflake $out/snowflake
        '';
    }
